<div class="main">
    <!-- MAIN CONTENT WRAP -->
    <div class="container g-0">
        <div class="content">
            <div class="row">
                <!-- LEFT -->
                <?php get_template_part('template-parts/home/content', 'left'); ?>
                <!-- LEFT -->

                <!-- CENTER -->
                <?php get_template_part('template-parts/home/content', 'center'); ?>
                <!-- CENTER -->

                <!-- RIGHT -->
                <?php get_template_part('template-parts/home/content', 'right'); ?>
                <!-- RIGHT -->

                <!-- BOTTOM -->
                <?php get_template_part('template-parts/home/content', 'bottom'); ?>
                <!-- BOTTOM -->
            </div>
        </div>
    </div>
    <!-- MAIN CONTENT WRAP -->
</div>