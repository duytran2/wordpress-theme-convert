<?php
$teachers = get_field('repeater-teacher', 'option');
$members = get_field('repeater-member', 'option');

$right_banner_gallery = get_field('banner-right');
?>

<div class="content__right col-sm-12 col-md col-lg-3">
    <?php if ($teachers) : ?>
        <div class="content__panel">
            <div class="content__panel-wrap">
                <h3 class="content__title">
                    MASTERS AND TEACHERS
                </h3>
                <div class="right__list-wrap">
                    <ul class="right__list list-unstyled">
                        <?php foreach ($teachers as $teacher) : ?>
                            <li class="right__list-item">
                                <a href="#" class="right__list-item__link link--format">
                                    <div class="item__wrap">
                                        <div class="right__img__wrap">
                                            <img src="<?php echo $teacher['avatar']; ?>" alt="" class="item__img" />
                                        </div>

                                        <div class="item__info">
                                            <span class="item__name">
                                                <?php echo $teacher['name']; ?>
                                            </span>
                                            <span class="item__description">
                                                <?php echo $teacher['introduce']; ?>
                                            </span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
        </div>
    <?php endif; ?>

    <?php if ($members) : ?>
        <div class="content__panel">
            <div class="content__panel-wrap">
                <h3 class="content__title">
                    Member Federation
                </h3>
                <div class="right__list-wrap">
                    <ul class="right__list list-unstyled">
                        <?php foreach ($members as $member) : ?>
                            <li class="member-list__item">
                                <a href="#" class="member-item__link link--format">
                                    <?php echo $member['name']; ?>
                                </a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
        </div>
    <?php endif; ?>

    <?php if ($right_banner_gallery) : ?>
        <?php foreach ($right_banner_gallery as $image) : ?>
            <div class="banner">
                <div class="banner__wrap banner__wrap--<?php echo $image['size']; ?>">
                    <img src="<?php echo $image['right-image-url']; ?>" alt="" class="banner__img">
                </div>
            </div>
        <?php endforeach; ?>
    <?php endif; ?>
</div>