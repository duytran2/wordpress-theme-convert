<?php
$social_icon = get_field('repeater-social-icon', 'option');

$left_banner_gallery = get_field('banner-left');
?>

<div class="content__left col-sm-12 col-md col-lg-3">
    <?php if ($social_icon) : ?>
        <div class="content__panel">
            <div class="content__panel-wrap">
                <h3 class="content__title">Follow us on</h3>
                <ul class="connect__list list-unstyled my-0">
                    <?php foreach ($social_icon as $icon) : ?>
                        <li class="connect__list-item">
                            <div class="connect__logo">
                                <a href="<?php echo $icon['permalink']; ?>" class="connect__logo-link" target="_blank">
                                    <img class="connect__logo-img" src="<?php echo $icon['icon-src']; ?>" alt="" />
                                </a>
                            </div>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
    <?php endif; ?>


    <div class="content__panel content__panel--background">
        <div class="content__panel-wrap">
            <h3 class="content__title">
                VOVINAM PHILOSOPHY
            </h3>
            <p class="content__left-description">
                <?php echo get_field('philosophy-content', 'option'); ?>
            </p>

            <div class="content__left-background">
                <img class="content__left-background-img" src="<?php echo get_field('philosophy-image', 'option'); ?>" alt="">
            </div>
        </div>
    </div>

    <?php if ($left_banner_gallery) : ?>
        <?php foreach ($left_banner_gallery as $image) : ?>
            <div class="banner">
                <div class="banner__wrap banner__wrap--<?php echo $image['size']; ?>">
                    <img src="<?php echo $image['left-image-url']; ?>" alt="" class="banner__img">
                </div>
            </div>
        <?php endforeach; ?>
    <?php endif; ?>
</div>