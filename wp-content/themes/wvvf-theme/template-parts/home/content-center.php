<div class="content__center col-sm-12 col-md-12 col-lg-6">
    <!-- SWIPER -->
    <?php get_template_part('template-parts/slider'); ?>
    <!-- SWIPER -->

    <!-- CENTER CONTENT -->
    <div class="content__panel">
        <div class="center__panel-wrap">
            <div class="center__header">
                <div class="center__header-tag"></div>
                <h1 class="center__header-title">
                    latest news
                </h1>
            </div>

            <?php
            $args = array(
                'category_name' => 'news',
                'orderby' => 'date',
                'order'   => 'DESC',
                'posts_per_page' => 5,
            );

            // Get data query
            $query_result = new WP_Query($args);
            $post_array = $query_result->posts;
            ?>

            <div id="mainContentCenter" class="row mt-12">
                <?php foreach ($post_array as $i => $post) :
                    // start content
                    if ($i == 0) :
                        echo '<div class="col-12">
                                            <div class="center__item active">';
                    else :
                        echo '<div class="col-lg-6 col-md-6 col-sm-6 col-12">
                                            <div class="center__item">';
                    endif;
                ?>
                    <!-- Permalink -->
                    <a href="<?php echo get_permalink(); ?>" class="center__item--link link--format">
                        <div class="center__wrap">
                            <div class="center__tag">
                                <!-- Post tag -->
                                <?php
                                $tags = get_the_tags();

                                foreach ($tags as $tag) {
                                    echo $tag->name;
                                }
                                ?>
                                <!-- Post tag -->
                            </div>

                            <div class="center__wrap-img">
                                <!-- Post thumbnail -->
                                <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="" class="center__img" />
                                <!-- Post thumbnail -->
                            </div>

                            <h3 class="center__title">
                                <!-- Post title -->
                                <?php echo get_the_title(); ?>
                                <!-- Post title -->
                            </h3>

                            <div class="center__info">
                                <p class="center__time">
                                    <!-- Post on -->
                                    <?php echo get_the_date('d F Y'); ?>
                                    <!-- Post on -->
                                </p>
                                <p class="center__author">
                                    <!-- Post by -->
                                    <?php
                                    $author_id = $post->post_author;
                                    echo get_the_author_meta('display_name', $author_id);
                                    ?>
                                    <!-- Post by -->
                                </p>
                            </div>

                            <p class="center__description">
                                <!-- Post excerpt -->
                                <?php echo get_the_excerpt(); ?>
                                <!-- Post excerpt -->
                            </p>
                        </div>
                    </a>
                    <!-- Permalink -->
                <?php
                    // end content
                    echo '</div>
                    </div>';
                endforeach;
                ?>
            </div>

            <div id="loadMore" class="more-btn__wrap">
                <a type="button" href="<?php echo get_permalink(get_page_by_title('News')); ?>" class="link--format more-btn">
                    <span class="more-btn__text">
                        see more news
                    </span>
                </a>
            </div>
        </div>
    </div>

    <div class="content__panel">
        <div class="center__panel-wrap">
            <div class="center__header">
                <div class="center__header-tag"></div>
                <h1 class="center__header-title">
                    videos
                </h1>
            </div>

            <?php
            $args = array(
                'post_type' => 'clip',
                'orderby' => 'date',
                'order'   => 'DESC',
                'posts_per_page' => 1,
            );

            // Get data query
            $query_result = new WP_Query($args);
            $post = $query_result->post;
            ?>

            <div id="" class="row mt-12">
                <div class="col-12">
                    <div class="center__item">
                        <!-- Permalink -->
                        <a href="<?php echo get_permalink(); ?>" class="center__item--link link--format">
                            <div class="center__wrap">
                                <div class="center__wrap-img">
                                    <!-- Post thumbnail -->
                                    <iframe src="<?php echo convertYoutubeUrl('https://www.youtube.com/watch?v=BAwm33bvFcA'); ?>" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen class="center__video"></iframe>
                                    <!-- Post thumbnail -->
                                </div>
                            </div>
                        </a>
                        <!-- Permalink -->
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="content__panel">
        <div class="center__panel-wrap">
            <div class="center__header">
                <div class="center__header-tag"></div>
                <h1 class="center__header-title">
                    techniques
                </h1>
            </div>

            <div id="" class="row mt-12">
                <div class="col-12">
                    <div class="center__item">
                        <!-- Permalink -->
                        <a href="<?php echo get_permalink(); ?>" class="center__item--link link--format">
                            <div class="center__wrap">
                                <div class="center__wrap-img">
                                    <!-- Post thumbnail -->
                                    <iframe src="<?php echo convertYoutubeUrl('https://www.youtube.com/watch?v=BAwm33bvFcA'); ?>" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen class="center__video"></iframe>
                                    <!-- Post thumbnail -->
                                </div>

                                <h3 class="center__title">
                                    <!-- Post title -->
                                    <?php echo get_the_title(); ?>
                                    <!-- Post title -->
                                </h3>

                                <div class="center__info">
                                    <p class="center__time">
                                        <!-- Post on -->
                                        <?php echo get_the_date('d F Y'); ?>
                                        <!-- Post on -->
                                    </p>
                                    <p class="center__author">
                                        <!-- Post by -->
                                        <?php
                                        $author_id = $post->post_author;
                                        echo get_the_author_meta('display_name', $author_id);
                                        ?>
                                        <!-- Post by -->
                                    </p>
                                </div>

                                <p class="center__description center__description--show">
                                    <!-- Post excerpt -->
                                    <?php echo get_the_excerpt(); ?>
                                    <!-- Post excerpt -->
                                </p>
                            </div>
                        </a>
                        <!-- Permalink -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- CENTER CONTENT -->
</div>

<?php wp_reset_postdata(); ?>