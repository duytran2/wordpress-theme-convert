<?php
$sponsor_list = get_field('sponsor-list');
?>
<?php if ($sponsor_list) : ?>
    <div class="content__center col-12">
        <div class="content__panel">
            <div class="bottom__panel-wrap">
                <h3 class="bottom__title">
                    sponsors
                </h3>

                <ul class="sponsor__list">
                    <?php foreach ($sponsor_list as $sponsor) : ?>
                        <li class="sponsor__item">
                            <img src="<?php echo $sponsor['sponsor-logo']; ?>" alt="" class="sponsor__img">
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
    </div>
<?php endif; ?>

<?php wp_reset_postdata(); ?>