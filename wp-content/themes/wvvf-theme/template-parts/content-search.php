<?php

/**
 * Template part for displaying results in search pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package wvvf-theme
 */
?>

<div class="content__center col-sm-12 col-md-12 col-lg-9">
	<!-- SWIPER -->
	<?php get_template_part('template-parts/slider'); ?>
	<!-- SWIPER -->

	<!-- CENTER CONTENT -->
	<div class="content__panel">
		<div class="center__panel-wrap">
			<div class="center__header">
				<div class="center__header-tag"></div>
				<h1 class="center__header-title">
					<?php printf(esc_html__('Search Results for: %s', 'wvvf-theme'), get_search_query()); ?>
				</h1>
			</div>

			<?php
			$paged = (get_query_var('page')) ? get_query_var('page') : 1;
			$args = array(
				'post_type' => array('gallery', 'post'),
				'orderby' => 'date',
				'order'   => 'DESC',
				'posts_per_page' => 9,
				'paged'          => $paged,
				's' => get_search_query()
			);

			$query_result = new WP_Query($args);
			$post_array = $query_result->posts;
			?>

			<div id="mainContentCenter" class="row mt-3">
				<?php foreach ($post_array as $i => $post) :
					// start content
					echo '<div class="col-lg-4 col-md-4 col-sm-4 col-12">
                            <div class="center__item">';
				?>
					<a href="<?php echo get_permalink(); ?>" class="center__item--link link--format">
						<div class="center__wrap">
							<div class="center__tag">
								<?php
								$tags = get_the_tags();

								foreach ($tags as $tag) {
									echo $tag->name;
								}
								?>
							</div>
							<div class="center__wrap-img">
								<img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="" class="center__img" />
							</div>
							<h3 class="center__title center__title--unwrap">
								<?php echo get_the_title(); ?>
							</h3>
							<div class="center__info">
								<p class="center__time">
									<?php echo get_the_date('d F Y'); ?>
								</p>
								<p class="center__author">
									<?php
									$author_id = $post->post_author;
									echo get_the_author_meta('display_name', $author_id);
									?>
								</p>
							</div>
							<p class="center__description center__description--show">
								<?php echo get_the_excerpt(); ?>
							</p>
						</div>
					</a>
				<?php
					// end content
					echo '</div>
                    </div>';
				endforeach;
				wp_reset_query();
				?>
			</div>

			<?php the_posts_pagination(array(
				'base'               => '%_%',
				'format'             => '?page=%#%',
				'total'              => $query_result->max_num_pages,
				'current'            => $paged,
				'show_all'           => false,
				'end_size'           => 1,
				'prev_next'          => true,
				'prev_text'          => __('<i class="las la-chevron-left"></i>'),
				'next_text'          => __('<i class="las la-chevron-right"></i>'),
				'type'               => 'plain',
				'add_args'           => false,
				'add_fragment'       => '',
				'before_page_number' => '',
				'after_page_number'  => ''
			)); ?>

		</div>
	</div>
	<!-- CENTER CONTENT -->
</div>

<?php get_template_part('template-parts/content-lastest', 'right') ?>