<div class="main">
    <!-- MAIN CONTENT WRAP -->
    <div class="container g-0">
        <div class="content">
            <div class="row">
                <!-- MAIN -->
                <?php
                if (is_single()) {
                    get_template_part('template-parts/gallery/content-post', 'main');
                } else {
                    get_template_part('template-parts/gallery/content', 'main');
                }
                ?>
                <!-- MAIN -->

                <!-- RIGHT -->
                <?php get_template_part('template-parts/gallery/content', 'right') ?>
                <!-- RIGHT -->
            </div>
        </div>
    </div>
    <!-- MAIN CONTENT WRAP -->
</div>