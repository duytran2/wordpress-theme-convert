<?php
define('FIRST_POST_INDEX', 0);
define('SECOND_POST_INDEX', 1);

function display_see_more_button($id_btn = '')
{
    echo '
    <div id="' . $id_btn . 'Wrap" class="more-btn__wrap">
        <a  id="' . $id_btn . '" type="button" href="' . get_permalink(get_page_by_title('News')) . '" class="link--format more-btn ">
            <span class="more-btn__text">
                see more news
            </span>    
        </a>
    </div>
    ';
}
?>


<div class="content__center col-sm-12 col-md-12 col-lg-9">
    <!-- CONTENT -->

    <!-- in other news -->
    <?php get_template_part('template-parts/news/content', 'other-news'); ?>
    <!-- in other news -->

    <!-- History of wvvf -->
    <?php get_template_part('template-parts/news/content', 'history'); ?>
    <!-- History of wvvf -->

    <!-- Lastest stories -->
    <?php get_template_part('template-parts/news/content', 'lastest-story'); ?>
    <!-- Lastest stories -->

    <!-- CONTENT -->
</div>