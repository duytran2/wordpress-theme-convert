<?php
$args = array(
    'category_name' => 'story',
    'orderby' => 'date',
    'order'   => 'DESC',
    'posts_per_page' => 3,
);

// Get data query
$query_result = new WP_Query($args);
$post_array = $query_result->posts;

$first_post = $post_array[FIRST_POST_INDEX];

unset($post_array[0]); // Remove first post in array

$social_icon = get_field('repeater-social-icon', 'option');

function filter_icon($var)
{
    return ($var['name'] === 'Facebook' || $var['name'] === 'Twitter' || $var['name'] === 'Instagram');
}

$social_icon_filtered = array_filter($social_icon, 'filter_icon');
?>
<div class="content__panel">
    <div class="center__panel-wrap">
        <div class="center__header">
            <div class="center__header-tag"></div>
            <h1 class="center__header-title">
                lastest stories
            </h1>
        </div>

        <?php if ($post_array) : ?>
            <div id="lastestStoryContent" class="row mt-12">
                <div class="col-lg-8 col-md-8 col-sm-8 col-12">
                    <div class="story__panel">
                        <a href="<?php echo get_permalink($first_post); ?>" class="">
                            <div class="story__background-wrap">
                                <img src="<?php echo get_the_post_thumbnail_url($first_post); ?>" class="story__background-img">
                            </div>

                            <div class="story__content">
                                <div class="story__content-tag">
                                    <span>
                                        <!-- First post tag -->
                                        <?php
                                        $tags = get_the_tags($first_post);

                                        foreach ($tags as $tag) {
                                            echo $tag->name;
                                        }
                                        ?>
                                        <!-- First post tag -->
                                    </span>
                                </div>

                                <div class="story__content-title">
                                    <?php echo get_the_title($first_post); ?>
                                </div>

                                <div class="story__content-info">
                                    <div class="story__content-info__left">
                                        <span class="story__content-info__label">
                                            <!-- First post info -->
                                            By <?php
                                                $first_author_id = $first_post->post_author;
                                                echo get_the_author_meta('display_name', $author_id);
                                                ?>
                                            on <?php echo get_the_date('d F Y', $first_post);   ?>
                                            <!-- First post info -->
                                        </span>
                                    </div>

                                    <div class="story__content-info__right">
                                        <span class="story__content-info__label">
                                            Share on:
                                        </span>

                                        <!-- First post sharing -->
                                        <?php foreach ($social_icon_filtered as $icon) : ?>
                                            <span class="story__content-info__icon">
                                                <img src="<?php echo $icon['icon-src']; ?>" alt="" class="story__content-info__img">
                                            </span>
                                        <?php endforeach; ?>
                                        <!-- First post sharing -->
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-4 col-12">
                    <?php foreach ($post_array as $post) : ?>
                        <div class="center__item">
                            <!-- Permalink -->
                            <a href="<?php echo get_permalink(); ?>" class="center__item--link link--format">
                                <div class="center__wrap">
                                    <div class="center__tag">
                                        <!-- Post tag -->
                                        <?php
                                        $tags = get_the_tags();

                                        foreach ($tags as $tag) {
                                            echo $tag->name;
                                        }
                                        ?>
                                        <!-- Post tag -->
                                    </div>

                                    <div class="center__wrap-img">
                                        <!-- Post thumbnail -->
                                        <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="" class="center__img" />
                                        <!-- Post thumbnail -->
                                    </div>

                                    <h3 class="center__title">
                                        <!-- Post title -->
                                        <?php echo get_the_title(); ?>
                                        <!-- Post title -->
                                    </h3>

                                    <div class="center__info">
                                        <p class="center__time">
                                            <!-- Post on -->
                                            <?php echo get_the_date('d-F-Y'); ?>
                                            <!-- Post on -->
                                        </p>
                                        <p class="center__author">
                                            <!-- Post by -->
                                            <?php
                                            $author_id = $post->post_author;
                                            echo get_the_author_meta('display_name', $author_id);
                                            ?>
                                            <!-- Post by -->
                                        </p>
                                    </div>

                                    <p class="center__description center__description--show">
                                        <!-- Post excerpt -->
                                        <?php echo get_the_excerpt(); ?>
                                        <!-- Post excerpt -->
                                    </p>
                                </div>
                            </a>
                            <!-- Permalink -->
                        </div>
                    <?php
                    endforeach;
                    wp_reset_query();
                    ?>
                </div>
            </div>

            <?php display_see_more_button('storyLoadMoreBtn'); ?>

        <?php endif; ?>

    </div>
</div>