<div class="main">
    <!-- MAIN CONTENT WRAP -->
    <div class="container g-0">
        <div class="content">
            <div class="row">
                <!-- CENTER -->
                <div class="content__center col-sm-12 col-md-12 col-lg-9">
                    <?php
                    $social_icon = get_field('repeater-social-icon', 'option');

                    function filter_icon($var)
                    {
                        return ($var['name'] === 'Facebook' || $var['name'] === 'Twitter' || $var['name'] === 'Instagram');
                    }

                    $social_icon_filtered = array_filter($social_icon, 'filter_icon');

                    $author_id = $post->post_author;
                    $banner_image = get_field('banner-image', 'option');
                    ?>

                    <?php if (is_page('contact')) : ?>
                        <div class="content__panel">
                            <div class="content__panel-wrap">
                                <h3 class="content__title">contact</h3>

                                <div class="row">
                                    <!-- Contact form -->
                                    <div class="col-sm-12 col-md col-lg-6">
                                        <?php echo the_content(); ?>

                                    </div>
                                    <!-- Contact form -->

                                    <!-- Google map -->
                                    <div class="col-sm-12 col-md col-lg-6">
                                        <div class="mapouter mt-12">
                                            <div class="gmap_canvas"><iframe width="100%" height="100%" id="gmap_canvas" src="https://maps.google.com/maps?q=31%20s%C6%B0%20v%E1%BA%A1n%20h%E1%BA%A1nh&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><a href="https://123movies-to.org"></a><br>
                                                <style>
                                                    .mapouter {
                                                        position: relative;
                                                        text-align: right;
                                                        height: 100%;
                                                        width: 100%;
                                                    }
                                                </style><a href="https://www.embedgooglemap.net">embedgooglemap.net</a>
                                                <style>
                                                    .gmap_canvas {
                                                        overflow: hidden;
                                                        background: none !important;
                                                        height: 500px;
                                                        width: 100%;
                                                    }
                                                </style>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Google map -->
                                </div>
                            </div>
                        </div>
                    <?php else : ?>
                        <!-- CENTER CONTENT -->
                        <div class="detail__content">
                            <div class="detail__panel-background" style="background: linear-gradient(180deg, rgba(124, 43, 43, 0) 0%, rgba(0, 155, 179, 0.93) 22.92%, #005360 100%),url(<?php echo $banner_image ?>);"></div>
                            <div class="detail__panel-wrap">
                                <div class="detail__content-tag">
                                    <p>
                                        Thế giới võ thuật
                                    </p>
                                </div>
                                <div class="detail__content-title">
                                    Tìm hiểu về võ Vovinam - Niềm tự hào của dân tộc Việt
                                </div>
                                <div class="detail__content-info">
                                    <div class="detail__content-info__left">
                                        <p class="detail__content-info__label">
                                            By Jo Crowley on 30 - Jul - 2020
                                        </p>
                                    </div>
                                    <div class="detail__content-info__right">
                                        <p class="detail__content-info__label">
                                            Share on:
                                        </p>

                                        <?php foreach ($social_icon_filtered as $icon) : ?>
                                            <span class="detail__content-info__icon">
                                                <img src="<?php echo $icon['icon-src']; ?>" alt="" class="detail__content-info__img">
                                            </span>
                                        <?php endforeach; ?>

                                    </div>
                                </div>
                                <div class="detail__content-img-wrap">
                                    <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="" class="detail__content-img">
                                </div>

                                <p class="detail__content-text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Saepe harum excepturi itaque doloribus minima sed nemo vitae, maxime repudiandae, eaque illo quasi possimus quos sit illum nam aspernatur odit eveniet rerum eligendi quis a nulla reprehenderit sequi. Modi, deserunt. Aspernatur, distinctio inventore quas tempora animi repudiandae corporis maxime mollitia neque.</p>

                                <h3 class="detail__content-heading">Medicine for headache</h3>

                                <p class="detail__content-text">
                                    Tincidunt dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet
                                </p>

                                <div class="detail__content-blackquote">
                                    <p class="detail__content-blackquote-text">Tincidunt dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi.</p>
                                </div>

                                <p class="detail__content-text">Tincidunt dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet</p>

                                <div class="detail__content-img-wrap detail__content-img-wrap--small ">
                                    <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="" class="detail__content-img">
                                </div>

                                <p class="detail__content-text">Tincidunt dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet</p>

                                <hr>

                                <ul class="list-tag">
                                    <li class="list-tag__item">Medicine</li>
                                    <li class="list-tag__item">Health</li>
                                    <li class="list-tag__item">Beauty</li>
                                    <li class="list-tag__item">Eye</li>
                                </ul>
                            </div>
                        </div>
                        <!-- CENTER CONTENT -->
                    <?php endif; ?>
                </div>
                <!-- CENTER -->

                <!-- RIGHT -->
                <?php get_template_part('template-parts/content-lastest', 'right') ?>
                <!-- RIGHT -->
            </div>
            <?php wp_reset_postdata(); ?>

        </div>
    </div>
    <!-- MAIN CONTENT WRAP -->
</div>