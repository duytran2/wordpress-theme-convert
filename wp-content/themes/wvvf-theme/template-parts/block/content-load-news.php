<div class="col-lg-4 col-md-4 col-sm-4 col-12">
    <div class="center__item">
        <!-- Permalink -->
        <a href="<?php echo get_permalink(); ?>" class="center__item--link link--format">
            <div class="center__wrap">
                <div class="center__tag">
                    <!-- Post tag -->
                    <?php
                    $tags = get_the_tags();

                    foreach ($tags as $tag) {
                        echo $tag->name;
                    }
                    ?>
                    <!-- Post tag -->
                </div>

                <div class="center__wrap-img">
                    <!-- Post thumbnail -->
                    <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="" class="center__img" />
                    <!-- Post thumbnail -->
                </div>

                <h3 class="center__title">
                    <!-- Post title -->
                    <?php echo get_the_title(); ?>
                    <!-- Post title -->
                </h3>

                <div class="center__info">
                    <p class="center__time">
                        <!-- Post on -->
                        <?php echo get_the_date('d F Y'); ?>
                        <!-- Post on -->
                    </p>
                    <p class="center__author">
                        <!-- Post by -->
                        <?php
                        $author_id = $post->post_author;
                        echo get_the_author_meta('display_name', $author_id);
                        ?>
                        <!-- Post by -->
                    </p>
                </div>

                <p class="center__description center__description--show">
                    <!-- Post excerpt -->
                    <?php echo get_the_excerpt(); ?>
                    <!-- Post excerpt -->
                </p>
            </div>
        </a>
        <!-- Permalink -->
    </div>
</div>