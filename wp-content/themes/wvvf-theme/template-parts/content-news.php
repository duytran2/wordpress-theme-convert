<div class="main">
    <!-- MAIN CONTENT WRAP -->
    <div class="container g-0">
        <div class="content">
            <div class="row">
                <!-- MAIN -->
                <?php get_template_part('template-parts/news/content', 'main'); ?>
                <!-- MAIN -->

                <!-- RIGHT -->
                <?php get_template_part('template-parts/content-lastest', 'right'); ?>
                <!-- RIGHT -->
            </div>
        </div>
    </div>
    <!-- MAIN CONTENT WRAP -->
</div>