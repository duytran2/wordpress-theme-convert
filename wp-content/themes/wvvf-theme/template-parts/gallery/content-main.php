<?php
$args = array(
    'post_type' => 'gallery',
    'orderby' => 'date',
    'order'   => 'DESC',
);

if (is_tax('type', 'image')) {
    $args = array(
        'post_type' => 'gallery',
        'tax_query' => array(
            array(
                'taxonomy' => 'type',
                'field'    => 'slug',
                'terms'    => 'image',
            ),
        ),
    );
}

if (is_tax('type', 'video')) {
    $args = array(
        'post_type' => 'gallery',
        'tax_query' => array(
            array(
                'taxonomy' => 'type',
                'field'    => 'slug',
                'terms'    => 'video',
            ),
        ),
    );
}

// Post query
$query_gallery = new WP_Query($args);
$post_array = $query_gallery->posts;
?>

<div class="content__center col-sm-12 col-md-12 col-lg-9">
    <!-- MAIN CONTENT -->
    <div class="gallery__content-wrap">
        <?php $banner_image = get_field('banner-image', 'option'); ?>
        <div class="gallery__panel-background" style="background-image: linear-gradient(180deg, rgba(124, 43, 43, 0) 0%, rgba(0, 155, 179, 0.93) 22.92%,  #005360 100%), url(<?php echo $banner_image ?>);"></div>
        <div class="gallery__panel-wrap">
            <div class="gallery__banner-title">
                Gallery
            </div>
        </div>

        <div class="gallery__content">
            <?php if ($post_array) : ?>
                <div class="row">
                    <?php foreach ($post_array as $post) : ?>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-12">
                            <div class="center__item">
                                <!-- Post permalink -->
                                <a href="<?php echo get_permalink(); ?>" class="center__item--link link--format">
                                    <div class="center__wrap">
                                        <!-- Post tag -->
                                        <?php if (!is_tax('type', array('video', 'image'))) :  ?>
                                            <div class="center__tag center__tag--show">
                                                <?php
                                                $taxonomies = get_the_terms($post->ID, 'type');
                                                foreach ($taxonomies as $taxonomy) {

                                                    echo $taxonomy->name;
                                                }
                                                ?>
                                            </div>
                                        <?php endif;  ?>
                                        <!-- Post tag -->

                                        <!-- Post thumbnail -->
                                        <div class="center__wrap-img">
                                            <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="" class="center__img" />
                                        </div>
                                        <!-- Post thumbnail -->

                                        <!-- Post title -->
                                        <h3 class="center__title">
                                            <?php echo get_the_title(); ?>
                                        </h3>
                                        <!-- Post title -->

                                        <div class="center__info">
                                            <!-- Check type of post to show info -->
                                            <?php if (is_tax('type', 'video')) : ?>
                                                <!-- Count number video -->
                                                <div class="center__count">
                                                    <?php $video_array = get_field('video-meta'); ?>
                                                    <i class="las la-file-video"></i>
                                                    <span>
                                                        <?php
                                                        if (count($video_array) > 1) {
                                                            echo count($video_array) . ' videos';
                                                        } else {
                                                            echo count($video_array) . ' video';
                                                        }
                                                        ?>
                                                    </span>
                                                </div>
                                                <!-- Count number video -->
                                            <?php else : ?>
                                                <!-- Post on -->
                                                <p class="center__time">
                                                    <?php echo get_the_date('d F Y'); ?>
                                                </p>
                                                <!-- Post on -->

                                                <!-- Post by -->
                                                <p class="center__author">
                                                    <?php
                                                    $author_id = $post->post_author;
                                                    echo get_the_author_meta('display_name', $author_id);
                                                    ?>
                                                </p>
                                                <!-- Post by -->
                                            <?php endif; ?>
                                            <!-- Check type of post to show info -->
                                        </div>
                                    </div>
                                </a>
                                <!-- Post permalink -->
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
        </div>
        <?php wp_reset_query(); ?>
    </div>
    <!-- MAIN CONTENT -->
</div>