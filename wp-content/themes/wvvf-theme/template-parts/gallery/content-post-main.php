<div class="content__center col-sm-12 col-md-12 col-lg-9">
    <?php
    $social_icon = get_field('repeater-social-icon', 'option');

    // Callbackfilter icon
    function filter_icon($var)
    {
        return ($var['name'] === 'Facebook' || $var['name'] === 'Twitter' || $var['name'] === 'Instagram');
    }

    // Get data
    $social_icon_filtered = array_filter($social_icon, 'filter_icon');
    $author_id = $post->post_author;
    $banner_image = get_field('banner-image', 'option');

    ?>

    <!-- MAIN CONTENT -->
    <div class="gallery__content-wrap">

        <!-- [BANNER] -->
        <!-- banner background -->
        <div class="gallery__panel-background" style="background-image: linear-gradient(180deg, rgba(124, 43, 43, 0) 0%, rgba(0, 155, 179, 0.93) 22.92%,  #005360 100%), url(<?php echo $banner_image ?>)" ;></div>
        <!-- banner background -->

        <div class="gallery__panel-wrap">
            <div class="gallery__banner-title">
                <!-- Post title [banner] -->
                <?php echo get_the_title(); ?>
                <!-- Post title [banner] -->
            </div>

            <div class="gallery__banner-info">
                <div class="gallery__banner-info__left">
                    <!-- Post info [banner] [left] -->
                    <p class="gallery__banner-info__label">
                        By <?php echo get_the_author_meta('display_name', $author_id) ?> on <?php echo get_the_date('d-F-Y'); ?>
                    </p>
                    <!-- Post info [banner] [left] -->
                </div>

                <div class="gallery__banner-info__right">
                    <!-- Post info [banner] [right] -->
                    <p class="gallery__banner-info__label">
                        Share on:
                    </p>

                    <?php foreach ($social_icon_filtered as $icon) : ?>
                        <span class="gallery__banner-info__icon">
                            <img src="<?php echo $icon['icon-src']; ?>" alt="" class="gallery__banner-info__img">
                        </span>
                    <?php endforeach; ?>
                    <!-- Post info [banner] [right] -->
                </div>
            </div>
        </div>
        <!-- [BANNER] -->

        <!-- Check post type is image or video -->
        <?php if (has_term('image', 'type')) : ?>

            <!-- Get image list -->
            <?php $images = get_field('image-list'); ?>

            <!-- If image list exist -->
            <?php if ($images) : ?>
                <div class="gallery__content">
                    <div class="swiper" id="swiperGallery">

                        <div class="swiper-wrapper">
                            <!-- Swiper image -->
                            <?php foreach ($images as $image) : ?>
                                <div class="swiper-slide gallery__swiper-slide">
                                    <img src="<?php echo $image ?>" alt="" class="slider__img" />
                                </div>
                            <?php endforeach; ?>
                            <!-- Swiper image -->
                        </div>

                        <!-- Navigation button swiper -->
                        <div class="custom-button-prev">
                            <i class="las la-angle-left"></i>
                        </div>
                        <div class="custom-button-next">
                            <i class="las la-angle-right"></i>
                        </div>
                        <!-- Navigation button swiper -->

                    </div>
                    <!-- Pagination -->
                    <div class="swiper-pagination gallery__swiper-pagination"></div>
                    <!-- Pagination -->
                </div>
            <?php endif; ?>
            <!-- If image list exist -->

        <?php else : ?>

            <!-- Get video list -->
            <?php $videos = get_field('video-meta'); ?>

            <!-- If video list exist -->
            <?php if ($videos) : ?>
                <div class="gallery__content">
                    <div class="row">

                        <!-- Loop video attachment post -->
                        <?php foreach ($videos as $video) : ?>
                            <!-- Item video -->
                            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                                <div class="center__item">
                                    <div class="center__wrap">
                                        <!-- Post thumbnail -->
                                        <div class="center__wrap-img">
                                            <iframe src="<?php echo convertYoutubeUrl($video['video-url']); ?>" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen class="center__img"></iframe>
                                        </div>
                                        <!-- Post thumbnail -->

                                        <!-- Post title -->
                                        <h3 class="center__title">
                                            <?php echo $video['name']; ?>
                                        </h3>
                                        <!-- Post title -->

                                        <div class="center__info">
                                            <!-- Count number video -->
                                            <div class="center__count">
                                                <i class="lar la-eye"></i>
                                                <span>
                                                    1 view
                                                </span>
                                            </div>
                                            <!-- Count number video -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Item video -->
                        <?php endforeach; ?>
                        <!-- Loop video attachment post -->

                    </div>
                </div>
            <?php endif; ?>
            <!-- If video list exist -->

        <?php endif; ?>
        <!-- Check post type is image or video -->

    </div>
    <!-- MAIN CONTENT -->
</div>