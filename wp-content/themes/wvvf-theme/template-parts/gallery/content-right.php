<?php
// Arguments to get all post of gallery
$args = array(
    'post_type' => 'gallery',
);

// Arguments to get image of gallery
if (has_term('image', 'type')) {
    $args = array(
        'post_type' => 'gallery',
        'tax_query' => array(
            array(
                'taxonomy' => 'type',
                'field'    => 'slug',
                'terms'    => 'image',
            ),
        ),
        'orderby' => 'date',
        'order'   => 'DESC',
    );
}

// Arguments to get video of gallery
if (has_term('video', 'type')) {
    $args = array(
        'post_type' => 'gallery',
        'tax_query' => array(
            array(
                'taxonomy' => 'type',
                'field'    => 'slug',
                'terms'    => 'video',
            ),
        ),
        'orderby' => 'date',
        'order'   => 'DESC',
    );
}

// Query get post
$query_gallery = new WP_Query($args);
$post_array = $query_gallery->posts;

?>

<div class="content__right col-sm-12 col-md col-lg-3">
    <div class="content__panel">
        <div class="content__panel-wrap">
            <h3 class="content__title">
                <!-- Label video/image page -->
                <?php if (has_term('video', 'type')) {
                    echo 'videos gallery';
                } else {
                    echo 'lastest gallery';
                } ?>
                <!-- Label video/image page -->
            </h3>

            <!-- Check have post -->
            <?php if ($post_array) : ?>
                <div class="row">
                    <?php foreach ($post_array as $post) : ?>

                        <!-- Post item layout -->
                        <div class="col-lg col-md col-sm col-12">
                            <div class="center__item">
                                <!-- Post permalink -->
                                <a href="<?php echo get_permalink(); ?>" class="center__item--link link--format">
                                    <div class="center__wrap">
                                        <!-- Thumbnail -->
                                        <div class="center__wrap-img">
                                            <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="" class="center__img" />
                                        </div>
                                        <!-- Thumbnail -->

                                        <!-- Post title -->
                                        <h3 class="center__title">
                                            <?php echo get_the_title(); ?>
                                        </h3>
                                        <!-- Post title -->

                                        <div class="center__info">
                                            <!-- Post on -->
                                            <p class="center__time">
                                                <?php echo get_the_date('d F Y'); ?>
                                            </p>
                                            <!-- Post on -->

                                            <!-- Post by -->
                                            <p class="center__author">
                                                <?php
                                                $author_id = $post->post_author;
                                                echo get_the_author_meta('display_name', $author_id);
                                                ?>
                                            </p>
                                            <!-- Post by -->
                                        </div>
                                    </div>
                                </a>
                                <!-- Post permalink -->
                            </div>
                        </div>
                        <!-- Post item layout -->

                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
            <!-- Check have post -->
        </div>
    </div>
</div>