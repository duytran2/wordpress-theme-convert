<?php
$args = array(
    'category_name' => 'news',
    'posts_per_page' => 4
);

// Get post last news
$query_news = new WP_Query($args);
$post_array = $query_news->posts;

define('REPEAT_START', 1);
define('REPEAT_END', 2);
?>

<div class="content__right col-sm-12 col-md col-lg-3">
    <?php for ($i = REPEAT_START; $i <= REPEAT_END; $i++) : ?>
        <div class="content__panel">
            <div class="content__panel-wrap">
                <h3 class="content__title">
                    <?php
                    if ($i == REPEAT_END) {
                        echo 'most popular';
                    } else {
                        echo 'lastest news';
                    }
                    ?>
                </h3>
                <?php if ($post_array) : ?>
                    <div class="row">
                        <?php foreach ($post_array as $post) : ?>
                            <div class="col-lg-12 col-md-4 col-sm-6 col-12">
                                <div class="center__item">
                                    <!-- Post permalink -->
                                    <a href="<?php echo get_permalink(); ?>" class="center__item--link link--format">
                                        <div class="center__wrap">
                                            <!-- Thumbnail -->
                                            <div class="center__wrap-img">
                                                <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="" class="center__img" />
                                            </div>
                                            <!-- Thumbnail -->

                                            <!-- Post title -->
                                            <h3 class="center__title center__title--unwrap">
                                                <?php echo get_the_title(); ?>
                                            </h3>
                                            <!-- Post title -->

                                            <div class="center__info">
                                                <!-- Post on -->
                                                <p class="center__time">
                                                    <?php echo get_the_date('d F Y'); ?>
                                                </p>
                                                <!-- Post on -->

                                                <!-- Post by -->
                                                <p class="center__author">
                                                    <?php
                                                    $author_id = $post->post_author;
                                                    echo get_the_author_meta('display_name', $author_id);
                                                    ?>
                                                </p>
                                                <!-- Post by -->
                                            </div>
                                        </div>
                                    </a>
                                    <!-- Post permalink -->
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    <?php endfor; ?>
</div>