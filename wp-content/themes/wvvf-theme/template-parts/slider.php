<?php
$images = get_field('image-slider');
// echo '<pre>';
// print_r($images);
// echo '</pre>';
?>
<?php if ($images) : ?>
    <div class="swiper">
        <div class="swiper-wrapper">
            <?php foreach ($images as $image) : ?>
                <div class="slider__wrap swiper-slide">
                    <img src="<?php echo $image['url'] ?>" alt="" class="slider__img" />
                </div>
            <?php endforeach; ?>
        </div>
        <div class="swiper-pagination"></div>
        <div class="swiper-button-prev"></div>
        <div class="swiper-button-next"></div>
    </div>
<?php endif; ?>