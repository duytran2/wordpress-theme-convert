<?php

/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package wvvf-theme
 */

get_header();
?>

<?php get_template_part('template-parts/content', 'news') ?>
<?php
get_footer(); ?>
<script>
    var ajaxUrl = "<?php echo admin_url('admin-ajax.php'); ?>";

    const otherIndex = 0;
    const historyIndex = 1;
    const storyIndex = 2;

    // Store current page of each panel
    let currentPageObj = [{
        name: 'other',
        currentPage: 1
    }, {
        name: 'history',
        currentPage: 1
    }, {
        name: 'story',
        currentPage: 1
    }];

    // Add click listener
    $("#otherNewsLoadMoreBtn").click({
        action: 'other_news_load_more',
        pageObjIndex: otherIndex
    }, ajaxAction);

    $("#historyLoadMoreBtn").click({
        action: 'history_load_more',
        pageObjIndex: historyIndex
    }, ajaxAction);

    $("#storyLoadMoreBtn").click({
        action: 'story_load_more',
        pageObjIndex: storyIndex
    }, ajaxAction);

    function ajaxAction(event) {
        let action = event.data.action;
        let pageObjIndex = event.data.pageObjIndex;
        let buttonLoad = $(event.target).parent();
        let buttonLoadWrap = buttonLoad.parent();
        let contentWrap = buttonLoadWrap.prev();

        // Update current page
        currentPageObj[pageObjIndex].currentPage++;
        currentPage = currentPageObj[pageObjIndex].currentPage;

        event.preventDefault();

        // Add loading class
        buttonLoad.addClass('more-btn--loading');

        $.ajax({
            type: "POST",
            url: ajaxUrl,
            dataType: "json",
            data: {
                action: action,
                paged: currentPage,
            },
            success: function(res) {
                // Delay load data
                setTimeout(function() {
                    // Render data
                    contentWrap.append(res.html);

                    // Load effect
                    buttonLoad.removeClass('more-btn--loading');

                    // Hide load button
                    if (res.current >= res.max) {
                        buttonLoadWrap.hide();
                    }
                }, 1000);
            },
        });
    }
</script>