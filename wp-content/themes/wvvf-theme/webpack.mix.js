let mix = require('laravel-mix');
mix.sass('assets/scss/app.scss', 'assets/css/').options({
    processCssUrls: false
});