<?php
class Custom_Subnav_Walker extends Walker_Nav_Menu
{
    public function start_lvl(&$output, $depth = 0, $args = array())
    {
        $indent = str_repeat("\t", $depth);
        $output .= "\n$indent<ul class=\"subnav__list list-unstyled\">\n";
    }

    public function end_lvl(&$output, $depth = 0, $args = null)
    {
        $indent = str_repeat("\t", $depth);
        $output .= "$indent</ul>\n";
    }

    public function start_el(&$output, $data_object, $depth = 0, $args = null, $current_object_id = 0)
    {
        $indent = ($depth) ? str_repeat("\t", $depth) : '';
        $classes = empty($data_object->classes) ? array() : (array) $data_object->classes;
        $classes[] = 'menu-item-' . $data_object->ID;

        $id = apply_filters('nav_menu_item_id', 'menu-item-' . $data_object->ID, $data_object, $args, $depth);
        $id = $id ? ' id="' . esc_attr($id) . '"' : '';

        // Generate nav, subnav class
        if ($depth === 0) {
            $class_names = join(' ', apply_filters('nav_menu_css_class', array_filter($classes), $data_object, $args, $depth));
            $class_names = $class_names ? ' class="' . esc_attr($class_names) . ' navbar__item"' : '';
            $output .= $indent . '<li' . $id . $class_names . '>';
            $output .= '<div class="navbar__item__wrap w-100">';
        } else {
            $class_names = join(' ', apply_filters('nav_menu_css_class', array_filter($classes), $data_object, $args, $depth));
            $class_names = $class_names ? ' class="' . esc_attr($class_names) . ' subnav__item"' : '';
            $output .= $indent . '<li' . $id . $class_names . '>';
        }

        $atts = array();
        $atts['title']  = !empty($data_object->attr_title) ? $data_object->attr_title : '';
        $atts['target'] = !empty($data_object->target)     ? $data_object->target     : '';
        $atts['rel']    = !empty($data_object->xfn)        ? $data_object->xfn        : '';
        $atts['href']   = !empty($data_object->url)        ? $data_object->url        : '';


        $atts = apply_filters('nav_menu_link_attributes', $atts, $data_object, $args, $depth);

        $attributes = '';
        foreach ($atts as $attr => $value) {
            if (!empty($value)) {
                $value = ('href' === $attr) ? esc_url($value) : esc_attr($value);
                $attributes .= ' ' . $attr . '="' . $value . '"';
            }
        }

        // Generate link class
        $data_object_output = $args->before;
        if ($depth === 0) {
            $data_object_output .= '<a' . $attributes . ' class="link--format navbar__item-link">';
        } else {
            $data_object_output .= '<a' . $attributes . ' class="link--format subnav__link">';
        }

        $span_icon = '<span class="navbar__item-icon">
                    <i class="las la-angle-down navbar__item-icon--down"></i>
                </span>';

        /** This filter is documented in wp-includes/post-template.php */
        $data_object_output .= $args->link_before . apply_filters('the_title', $data_object->title, $data_object->ID) . $args->link_after;
        $data_object_output .= '</a>';

        // if data_object has children then insert icon
        if ($args->walker->has_children) {
            $data_object_output .= $span_icon;
        }

        if ($depth === 0) {
            $data_object_output .= "</div>\n";
        }
        $data_object_output .= $args->after;

        $output .= apply_filters('walker_nav_menu_start_el', $data_object_output, $data_object, $depth, $args);
    }

    public function end_el(&$output, $data_object, $depth = 0, $args = null)
    {
        $output .= "</li>\n";
    }
}
