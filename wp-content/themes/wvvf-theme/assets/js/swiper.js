// Swiper
const swiperHome = new Swiper(".swiper", {
    // Optional parameters
    loop: true,
    autoplay: false,
    autoplay: {
        delay: 5000,
        pauseOnMouseEnter: true,
        disableOnInteraction: false,
    },
    // If we need pagination
    pagination: {
        el: ".swiper-pagination",
        // bulletClass: 'test-bullet',
        // bulletActiveClass: 'test-bullet--active',
        clickable: true,
        // renderBullet: function (index, bulletClass) {
        //     return '<span class="' + bulletClass + '">' + (index + 1) + '</span>';
        // },
    },

    // Navigation arrows
    navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev",
    },
});

const swiperGallery = new Swiper("#swiperGallery", {
    // Optional parameters
    loop: true,
    autoplay: true,
    autoplay: {
        delay: 5000,
        pauseOnMouseEnter: true,
        disableOnInteraction: false,
    },
    // If we need pagination
    pagination: {
        el: ".swiper-pagination",
        // bulletClass: "gallery-bullet",
        // bulletActiveClass: "gallery-bullet--active",
        clickable: true,
        // renderBullet: function (index, bulletClass) {
        //     return (
        //         '<span class="' + bulletClass + '">' + (index + 1) + "</span>"
        //     );
        // },
    },

    // Navigation arrows
    navigation: {
        nextEl: ".custom-button-next",
        prevEl: ".custom-button-prev",
    },
});
