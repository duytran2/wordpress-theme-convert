$(document).ready(function () {
    var navBar = $('#navbarList')

    // Toggle menu
    $('#btnMenu').on('click', function () {
        navBar.toggleClass('active')
    })

    // Toggle submenu when mobile/tablet devide
    $('.navbar__item').on('click', function (e) {
        // e.preventDefault()
        // alert('click')
        $(".subnav__list").not($(this).children()).hide()
        $(this).children(".subnav__list").toggle()
    })

    // Click outside hide submenu
    $(this).on('click', function (e) {
        if (!$(e.target).closest(".navbar__item").length && !$(e.target).closest("#btnMenu").length) {
            $(".subnav__list").hide()
            navBar.removeClass('active')
        }
    })
});