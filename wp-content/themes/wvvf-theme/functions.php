<?php

/**
 * wvvf-theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package wvvf-theme
 */

if (!defined('_S_VERSION')) {
	// Replace the version number of the theme on each release.
	define('_S_VERSION', '1.0.0');
}

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function wvvf_theme_setup()
{
	/*
		* Make theme available for translation.
		* Translations can be filed in the /languages/ directory.
		* If you're building a theme based on wvvf-theme, use a find and replace
		* to change 'wvvf-theme' to the name of your theme in all the template files.
		*/
	load_theme_textdomain('wvvf-theme', get_template_directory() . '/languages');

	// Add default posts and comments RSS feed links to head.
	add_theme_support('automatic-feed-links');

	/*
		* Let WordPress manage the document title.
		* By adding theme support, we declare that this theme does not use a
		* hard-coded <title> tag in the document head, and expect WordPress to
		* provide it for us.
		*/
	add_theme_support('title-tag');

	/*
		* Enable support for Post Thumbnails on posts and pages.
		*
		* @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		*/
	add_theme_support('post-thumbnails');

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus(
		array(
			'primary_menu' => esc_html__('Primary', 'wvvf-theme'),
		)
	);

	/*
		* Switch default core markup for search form, comment form, and comments
		* to output valid HTML5.
		*/
	add_theme_support(
		'html5',
		array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
			'style',
			'script',
		)
	);

	// Set up the WordPress core custom background feature.
	add_theme_support(
		'custom-background',
		apply_filters(
			'wvvf_theme_custom_background_args',
			array(
				'default-color' => 'ffffff',
				'default-image' => '',
			)
		)
	);

	// Add theme support for selective refresh for widgets.
	add_theme_support('customize-selective-refresh-widgets');

	/**
	 * Add support for core custom logo.
	 *
	 * @link https://codex.wordpress.org/Theme_Logo
	 */
	add_theme_support(
		'custom-logo',
		array(
			'height'      => 92,
			'width'       => 92,
			'flex-width'  => true,
			'flex-height' => true,
		)
	);
}
add_action('after_setup_theme', 'wvvf_theme_setup');

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function wvvf_theme_content_width()
{
	$GLOBALS['content_width'] = apply_filters('wvvf_theme_content_width', 640);
}
add_action('after_setup_theme', 'wvvf_theme_content_width', 0);

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function wvvf_theme_widgets_init()
{
	register_sidebar(
		array(
			'name'          => esc_html__('Sidebar', 'wvvf-theme'),
			'id'            => 'sidebar-1',
			'description'   => esc_html__('Add widgets here.', 'wvvf-theme'),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action('widgets_init', 'wvvf_theme_widgets_init');

/**
 * Enqueue scripts and styles.
 */
function wvvf_theme_scripts()
{
	wp_enqueue_style('wvvf-theme-style', get_stylesheet_uri(), array(), _S_VERSION);
	wp_style_add_data('wvvf-theme-style', 'rtl', 'replace');

	wp_enqueue_script('wvvf-theme-navigation', get_template_directory_uri() . '/js/navigation.js', array(), _S_VERSION, true);

	if (is_singular() && comments_open() && get_option('thread_comments')) {
		wp_enqueue_script('comment-reply');
	}
}
add_action('wp_enqueue_scripts', 'wvvf_theme_scripts');

function wvvf_theme_style()
{
	wp_enqueue_style('wvvf-theme-app-style', get_template_directory_uri() . '/assets/css/app.css');
}
add_action('wp_enqueue_scripts', 'wvvf_theme_style');

function wvvf_theme_bootstrap()
{
	wp_register_style('wvvf-theme-style-bootstrap', 'https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css');
	wp_enqueue_style('wvvf-theme-style-bootstrap');

	wp_register_script('wvvf-theme-script-bootstrap', 'https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js', '', '', true);
	wp_enqueue_script('wvvf-theme-script-bootstrap');

	wp_register_script('wvvf-theme-script-popper', 'https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js', '', '', true);
	wp_enqueue_script('wvvf-theme-script-popper');
}
add_action('wp_enqueue_scripts', 'wvvf_theme_bootstrap');

function wvvf_theme_style_icon()
{
	wp_register_style('wvvf-theme-style-icon', 'https://maxst.icons8.com/vue-static/landings/line-awesome/line-awesome/1.3.0/css/line-awesome.min.css');
	wp_enqueue_style('wvvf-theme-style-icon');
}
add_action('wp_enqueue_scripts', 'wvvf_theme_style_icon');

function wvvf_theme_style_swiper()
{
	wp_register_style('wvvf-theme-style-swiper', 'https://unpkg.com/swiper/swiper-bundle.min.css');
	wp_enqueue_style('wvvf-theme-style-swiper');

	wp_register_script('wvvf-theme-script-swiper-library', 'https://unpkg.com/swiper/swiper-bundle.min.js', '', '', true);
	wp_enqueue_script('wvvf-theme-script-swiper-library');
	wp_enqueue_script('wvvf-theme-script-swiper', get_template_directory_uri() . '/assets/js/swiper.js', '', '', true);
}
add_action('wp_enqueue_scripts', 'wvvf_theme_style_swiper');

// Load more button script
function wvvf_theme_script_load_more()
{
	wp_register_script('wvvf-theme-script-jquery-library', 'https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js', '', '', true);
	wp_enqueue_script('wvvf-theme-script-jquery-library');

	wp_enqueue_script('wvvf-theme-script-other-news-load-more', get_template_directory_uri() . '/assets/js/main.js', '', '', true);
}
add_action('get_footer', 'wvvf_theme_script_load_more');
// Load more button script

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if (defined('JETPACK__VERSION')) {
	require get_template_directory() . '/inc/jetpack.php';
}

/*Adding class to menu item - li tag */
function add_menu_list_item_class($classes, $item, $args)
{
	if ($args->list_item_class) {
		$classes[] = $args->list_item_class;
	}
	return $classes;
}
add_filter('nav_menu_css_class', 'add_menu_list_item_class', 1, 3);
/*Adding class to link menu item - a tag */
function add_menu_link_class($atts, $item, $args)
{
	if ($args->link_class) {
		$atts['class'] = $args->link_class;
	}
	return $atts;
}
add_filter('nav_menu_link_attributes', 'add_menu_link_class', 1, 3);

// Register footer menu
function register_footer_menus()
{
	register_nav_menus(
		array(
			'additional-menu' => __('Footer Menu'),
			// 'extra-menu' => __('Extra Menu')
		)
	);
}
add_action('init', 'register_footer_menus');
// Register footer menu

// Duplicate post
function rd_duplicate_post_as_draft()
{
	global $wpdb;
	if (!(isset($_GET['post']) || isset($_POST['post'])  || (isset($_REQUEST['action']) && 'rd_duplicate_post_as_draft' == $_REQUEST['action']))) {
		wp_die('No post to duplicate has been supplied!');
	}
	/*
	 * Nonce verification
	 */
	if (!isset($_GET['duplicate_nonce']) || !wp_verify_nonce($_GET['duplicate_nonce'], basename(__FILE__)))
		return;
	/*
	 * get the original post id
	 */
	$post_id = (isset($_GET['post']) ? absint($_GET['post']) : absint($_POST['post']));
	/*
	 * and all the original post data then
	 */
	$post = get_post($post_id);
	/*
	 * if you don't want current user to be the new post author,
	 * then change next couple of lines to this: $new_post_author = $post->post_author;
	 */
	$current_user = wp_get_current_user();
	$new_post_author = $current_user->ID;
	/*
	 * if post data exists, create the post duplicate
	 */
	if (isset($post) && $post != null) {
		/*
	   * new post data array
	   */
		$args = array(
			'comment_status' => $post->comment_status,
			'ping_status'    => $post->ping_status,
			'post_author'    => $new_post_author,
			'post_content'   => $post->post_content,
			'post_excerpt'   => $post->post_excerpt,
			'post_name'      => $post->post_name,
			'post_parent'    => $post->post_parent,
			'post_password'  => $post->post_password,
			'post_status'    => 'draft',
			'post_title'     => $post->post_title,
			'post_type'      => $post->post_type,
			'to_ping'        => $post->to_ping,
			'menu_order'     => $post->menu_order
		);
		/*
	   * insert the post by wp_insert_post() function
	   */
		$new_post_id = wp_insert_post($args);
		/*
	   * get all current post terms ad set them to the new post draft
	   */
		$taxonomies = get_object_taxonomies($post->post_type); // returns array of taxonomy names for post type, ex array("category", "post_tag");
		foreach ($taxonomies as $taxonomy) {
			$post_terms = wp_get_object_terms($post_id, $taxonomy, array('fields' => 'slugs'));
			wp_set_object_terms($new_post_id, $post_terms, $taxonomy, false);
		}
		/*
	   * duplicate all post meta just in two SQL queries
	   */
		$post_meta_infos = $wpdb->get_results("SELECT meta_key, meta_value FROM $wpdb->postmeta WHERE post_id=$post_id");
		if (count($post_meta_infos) != 0) {
			$sql_query = "INSERT INTO $wpdb->postmeta (post_id, meta_key, meta_value) ";
			foreach ($post_meta_infos as $meta_info) {
				$meta_key = $meta_info->meta_key;
				if ($meta_key == '_wp_old_slug') continue;
				$meta_value = addslashes($meta_info->meta_value);
				$sql_query_sel[] = "SELECT $new_post_id, '$meta_key', '$meta_value'";
			}
			$sql_query .= implode(" UNION ALL ", $sql_query_sel);
			$wpdb->query($sql_query);
		}
		/*
	   * finally, redirect to the edit post screen for the new draft
	   */
		wp_redirect(admin_url('post.php?action=edit&post=' . $new_post_id));
		exit;
	} else {
		wp_die('Post creation failed, could not find original post: ' . $post_id);
	}
}
add_action('admin_action_rd_duplicate_post_as_draft', 'rd_duplicate_post_as_draft');
/*
   * Add the duplicate link to action list for post_row_actions
   */
function rd_duplicate_post_link($actions, $post)
{
	if (current_user_can('edit_posts')) {
		$actions['duplicate'] = '<a href="' . wp_nonce_url('admin.php?action=rd_duplicate_post_as_draft&post=' . $post->ID, basename(__FILE__), 'duplicate_nonce') . '" title="Duplicate this item" rel="permalink">Duplicate</a>';
	}
	return $actions;
}
add_filter('post_row_actions', 'rd_duplicate_post_link', 10, 2);
// Duplicate post

// Add option page
if (function_exists('acf_add_options_page')) {
	acf_add_options_page();
}
// Add option page

// CPT Gallery
function wpdocs_codex_gallery_init()
{
	$labels = array(
		'name'                  => _x('Gallery', 'Post type general name', 'textdomain'),
		'singular_name'         => _x('Gallery', 'Post type singular name', 'textdomain'),
		'menu_name'             => _x('Gallery', 'Admin Menu text', 'textdomain'),
		'name_admin_bar'        => _x('Gallery', 'Add New on Toolbar', 'textdomain'),
		'add_new'               => __('Add New', 'textdomain'),
		'add_new_item'          => __('Add New Gallery', 'textdomain'),
		'new_item'              => __('New Gallery', 'textdomain'),
		'edit_item'             => __('Edit Gallery', 'textdomain'),
		'view_item'             => __('View Gallery', 'textdomain'),
		'all_items'             => __('All Gallery', 'textdomain'),
		'search_items'          => __('Search Gallery', 'textdomain'),
		'parent_item_colon'     => __('Parent Gallery:', 'textdomain'),
		'not_found'             => __('No gallery found.', 'textdomain'),
		'not_found_in_trash'    => __('No gallery found in Trash.', 'textdomain'),
		'featured_image'        => _x('Gallery Cover Image', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'textdomain'),
		'set_featured_image'    => _x('Set cover image', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'textdomain'),
		'remove_featured_image' => _x('Remove cover image', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'textdomain'),
		'use_featured_image'    => _x('Use as cover image', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'textdomain'),
		'archives'              => _x('Gallery archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'textdomain'),
		'insert_into_item'      => _x('Insert into gallery', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'textdomain'),
		'uploaded_to_this_item' => _x('Uploaded to this gallery', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'textdomain'),
		'filter_items_list'     => _x('Filter gallery list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'textdomain'),
		'items_list_navigation' => _x('Gallery list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'textdomain'),
		'items_list'            => _x('Gallery list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'textdomain'),
	);

	$args = array(
		'labels'             => $labels,
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array('slug' => 'gallery/%type%', 'with_front' => false),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array('title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments', 'post-formats', 'revisions', 'custom-fields'),
	);

	register_post_type('gallery', $args);
}

add_action('init', 'wpdocs_codex_gallery_init');
// CPT Gallery

// CPT Clip
function wpdocs_codex_clip_init()
{
	$labels = array(
		'name'                  => _x('Clips', 'Post type general name', 'textdomain'),
		'singular_name'         => _x('Clip', 'Post type singular name', 'textdomain'),
		'menu_name'             => _x('Clips', 'Admin Menu text', 'textdomain'),
		'name_admin_bar'        => _x('Clip', 'Add New on Toolbar', 'textdomain'),
		'add_new'               => __('Add New', 'textdomain'),
		'add_new_item'          => __('Add New Clip', 'textdomain'),
		'new_item'              => __('New Clip', 'textdomain'),
		'edit_item'             => __('Edit Clip', 'textdomain'),
		'view_item'             => __('View Clip', 'textdomain'),
		'all_items'             => __('All Clips', 'textdomain'),
		'search_items'          => __('Search Clips', 'textdomain'),
		'parent_item_colon'     => __('Parent Clips:', 'textdomain'),
		'not_found'             => __('No clips found.', 'textdomain'),
		'not_found_in_trash'    => __('No clips found in Trash.', 'textdomain'),
		'featured_image'        => _x('Clip Cover Image', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'textdomain'),
		'set_featured_image'    => _x('Set cover image', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'textdomain'),
		'remove_featured_image' => _x('Remove cover image', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'textdomain'),
		'use_featured_image'    => _x('Use as cover image', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'textdomain'),
		'archives'              => _x('Clip archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'textdomain'),
		'insert_into_item'      => _x('Insert into clip', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'textdomain'),
		'uploaded_to_this_item' => _x('Uploaded to this clip', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'textdomain'),
		'filter_items_list'     => _x('Filter clips list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'textdomain'),
		'items_list_navigation' => _x('Clips list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'textdomain'),
		'items_list'            => _x('Clips list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'textdomain'),
	);

	$args = array(
		'labels'             => $labels,
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array('slug' => 'clip'),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array('title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments', 'post-formats', 'revisions', 'custom-fields'),
	);

	register_post_type('clip', $args);
}

add_action('init', 'wpdocs_codex_clip_init');
// CPT Gallery

// Taxonomy
function wpdocs_create_gallery_taxonomies()
{
	$labels = array(
		'name'              => _x('Types', 'taxonomy general name', 'textdomain'),
		'singular_name'     => _x('Type', 'taxonomy singular name', 'textdomain'),
		'search_items'      => __('Search Types', 'textdomain'),
		'all_items'         => __('All Types', 'textdomain'),
		'parent_item'       => __('Parent Type', 'textdomain'),
		'parent_item_colon' => __('Parent Type:', 'textdomain'),
		'edit_item'         => __('Edit Type', 'textdomain'),
		'update_item'       => __('Update Type', 'textdomain'),
		'add_new_item'      => __('Add New Type', 'textdomain'),
		'new_item_name'     => __('New Type Name', 'textdomain'),
		'menu_name'         => __('Type', 'textdomain'),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array('slug' => 'gallery'),
	);

	register_taxonomy('type', 'gallery', $args);
}

add_action('init', 'wpdocs_create_gallery_taxonomies', 0);
// Taxonomy

// Permalink taxonomy
add_filter('post_type_link', 'gallery_type_permalink_structure', 10, 4);
function gallery_type_permalink_structure($post_link, $post, $leavename, $sample)
{
	if (false !== strpos($post_link, '%type%')) {
		$gallery_type_term = get_the_terms($post->ID, 'type');
		if (!empty($gallery_type_term)) {
			$post_link = str_replace('%type%', array_pop($gallery_type_term)->slug, $post_link);
		} else {
			$post_link = str_replace('%type%', 'uncategorized', $post_link);
		}
	}
	return $post_link;
}
// Permalink taxonomy

// Load more news
// Other news
function other_news_load_more()
{
	$paged = $_POST['paged'];
	$html = '';

	$ajax_post = new WP_Query([
		'post_type' => 'post',
		'category_name' => 'news',
		'posts_per_page' => 6,
		'orderby' => 'date',
		'order' => 'DESC',
		'paged' => $paged,
	]);

	$max_pages = $ajax_post->max_num_pages;

	if ($ajax_post->have_posts()) {
		ob_start();
		while ($ajax_post->have_posts()) : $ajax_post->the_post();
			$html .= get_template_part('template-parts/block/content', 'load-news');
		endwhile;
		$output = ob_get_contents();
		ob_end_clean();
	} else {
		$html = '';
	}

	$result = [
		'current' => $paged,
		'max' => $max_pages,
		'html' => $output,
	];

	echo json_encode($result);
	exit;
}
add_action('wp_ajax_other_news_load_more', 'other_news_load_more');
add_action('wp_ajax_nopriv_other_news_load_more', 'other_news_load_more');
// Other news

// History
function history_load_more()
{
	$paged = $_POST['paged'];
	$html = '';

	$ajax_post = new WP_Query([
		'post_type' => 'post',
		'category_name' => 'history',
		'posts_per_page' => 6,
		'orderby' => 'date',
		'order' => 'DESC',
		'paged' => $paged,
	]);

	$max_pages = $ajax_post->max_num_pages;

	if ($ajax_post->have_posts()) {
		ob_start();
		while ($ajax_post->have_posts()) : $ajax_post->the_post();
			$html .= get_template_part('template-parts/block/content', 'load-news');
		endwhile;
		$output = ob_get_contents();
		ob_end_clean();
	} else {
		$html = '';
	}

	$result = [
		'current' => $paged,
		'max' => $max_pages,
		'html' => $output,
	];

	echo json_encode($result);
	exit;
}
add_action('wp_ajax_history_load_more', 'history_load_more');
add_action('wp_ajax_nopriv_history_load_more', 'history_load_more');
// History

// Story
function story_load_more()
{
	$paged = $_POST['paged'];
	$html = '';

	$ajax_post = new WP_Query([
		'post_type' => 'post',
		'category_name' => 'story',
		'posts_per_page' => 3,
		'orderby' => 'date',
		'order' => 'DESC',
		'paged' => $paged,
	]);

	$max_pages = $ajax_post->max_num_pages;

	if ($ajax_post->have_posts()) {
		ob_start();
		while ($ajax_post->have_posts()) : $ajax_post->the_post();
			$html .= get_template_part('template-parts/block/content', 'load-news');
		endwhile;
		$output = ob_get_contents();
		ob_end_clean();
	} else {
		$html = '';
	}

	$result = [
		'current' => $paged,
		'max' => $max_pages,
		'html' => $output,
	];

	echo json_encode($result);
	exit;
}
add_action('wp_ajax_story_load_more', 'story_load_more');
add_action('wp_ajax_nopriv_story_load_more', 'story_load_more');
// Story
// Load more news

// Custom offset
add_action('pre_get_posts', 'other_news_query_offset', 1);
function other_news_query_offset(&$query)
{
	//Before anything else, make sure this is the right query...
	if ($query->is_home()) {
		return;
	}

	//First, define your desired offset...
	$offset = 0;

	//Next, determine how many posts per page you want (we'll use WordPress's settings)
	$ppp = $query->query_vars['posts_per_page'];


	if ($query->is_admin && $query->query_vars['category_name'] == 'news') {
		//Next, detect and handle pagination...
		if ($query->is_paged) {

			//Manually determine page query offset (offset + current page (minus one) x posts per page)
			$page_offset = $offset + (($query->query_vars['paged'] - 1) * $ppp);

			//Apply adjust page offset
			$query->set('offset', $page_offset + 1);
		} else {

			//This is the first page. Just use the offset...
			$query->set('offset', 0);
		}
	}
}

add_filter('found_posts', 'other_news_adjust_offset_pagination', 1, 2);
function other_news_adjust_offset_pagination($found_posts, $query)
{
	//Define our offset again...
	$offset = 0;

	//Ensure we're modifying the right query object...
	if (!$query->is_home()) {
		//Reduce WordPress's found_posts count by the offset... 
		return $found_posts - $offset;
	}
	return $found_posts;
}
// Custom offset

// Global function
// Function convert link youtube to embed
function convertYoutubeUrl($string)
{
	return preg_replace(
		"/\s*[a-zA-Z\/\/:\.]*youtu(be.com\/watch\?v=|.be\/)([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i",
		"https://www.youtube.com/embed/$2",
		$string
	);
}
// Global function