<form id="searchform" class="search__form" method="get" action="<?php echo esc_url(home_url('/')); ?>">
    <input type="text" class="search search__field <?php if (is_404()) {
                                                        echo 'search__field--full';
                                                    } ?>" name="s" placeholder="Search something..." value="<?php echo get_search_query(); ?>">
    <button class="search search__btn" type="submit">
        <i class="las la-search"></i>
    </button>
</form>