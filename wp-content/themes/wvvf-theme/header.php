<?php

/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package wvvf-theme
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<?php wp_body_open(); ?>
	<div class="header">
		<div class="container g-0 h-100">
			<div class="header__wrap">
				<div class="header__logo">
					<?php
					$custom_logo_id = get_theme_mod('custom_logo');
					$image = wp_get_attachment_image_src($custom_logo_id, 'full');
					?>
					<a href="<?php echo esc_url(home_url('/')); ?>" class="header__logo-link">
						<img class="header__logo-image" src="<?php echo $image[0]; ?>" alt="">
					</a>
				</div>

				<div class="header__info w-100">
					<div class="header__top">
						<div class="header__brand-name">
							<?php echo get_bloginfo('name'); ?>
						</div>

						<div class="header__top-right">
							<?php get_search_form(); ?>

							<div class="select__lang-wrap">
								<i class="las la-globe"></i>
								<select id="lang" class="select__lang">
									<option class="option__lang" value="vn">VN</option>
									<option class="option__lang" value="en">EN</option>
									<option class="option__lang" value="fr">FR</option>
								</select>
							</div>
						</div>
					</div>


					<!-- <div class="header__navbar"> -->
					<div class="navbar__mini" id="btnMenu">
						<span class="navbar__mini__item-icon">
							<i class="las la-bars"></i>
						</span>
						<span class="navbar__mini__title">menu</span>
					</div>
					<?php
					require_once('Custom_Subnav_Walker.php');
					$custom_walker = new Custom_Subnav_Walker;
					wp_nav_menu(array(
						'theme_location'  => 'primary_menu',
						'container_class'  => 'header__navbar',
						'menu_id'  => 'navbarList',
						'menu_class'    => 'navbar__list',
						'walker' => $custom_walker
					));
					?>
				</div>
			</div>
		</div>
	</div>