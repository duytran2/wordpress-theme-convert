<?php

/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package wvvf-theme
 */

get_header();
?>

<div class="main">
	<!-- MAIN CONTENT WRAP -->
	<div class="container g-0">
		<div class="content">
			<div class="row">
				<?php get_template_part('template-parts/content', 'search') ?>
			</div>
		</div>
	</div>
	<!-- MAIN CONTENT WRAP -->
</div>

<?php
// get_sidebar();
get_footer();
