<?php

/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package wvvf-theme
 */

?>

<?php
// get id and src image
$custom_logo_id = get_theme_mod('custom_logo');
$image = wp_get_attachment_image_src($custom_logo_id, 'full');
$background_footer_image_url = get_theme_mod('header_image');
?>
<div class="footer" style="background-image: url(<?php echo $background_footer_image_url ?>), linear-gradient(274.3deg, #2a5ccc 24.41%, #0aadc6 93.2%);">
	<div class="footer__wrap">
		<div class="footer__body">

			<div class="footer__logo-wrap">
				<a href="<?php echo esc_url(home_url('/')); ?>">
					<img src="<?php echo $image[0]; ?>" alt="" class="footer__logo-img">
				</a>
			</div>

			<div class="footer__brand-name">
				<?php echo get_bloginfo('name'); ?>
			</div>

			<p class="footer__copyright">
				Copyright 2012 - 2020. All rights reserved.
			</p>

			<?php
			wp_nav_menu(array(
				'theme_location'  => 'additional-menu',
				'container' => '',
				'menu_class'    => 'footer__nav list-unstyled',
				'link_class'        => 'footer__nav-link link--format',
				'list_item_class'   => 'footer__nav-item'
			));
			?>
		</div>
	</div>
</div>
<?php wp_footer(); ?>

</body>

</html>