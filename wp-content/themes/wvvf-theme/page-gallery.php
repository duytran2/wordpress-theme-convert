<?php
// Layout for page gallery
?>
<?php get_header(); ?>

<?php
get_template_part('template-parts/content', 'gallery')
?>

<?php get_footer(); ?>